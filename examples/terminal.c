#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <esdb.h>
#include <block_list.h>

void add(struct esdb *db, long int entity_id, 
		int const **to_read, int **to_set) {
	**to_set = **to_read + entity_id;
}

void print(struct esdb *db, long int entity_id, 
		int const **to_read, int **to_set) {
	printf("%d\n", **to_read);
	**to_set = **to_read;
}

int main(void) {
    struct esdb *db = make_esdb(1, 1024);
    int values = register_component(db, sizeof(int));

	char input[4096] = "";
	int components[1] = { values };
	while (strcmp("exit\n", input) != 0) {
		fputs(":> ", stdout);
		fgets(input, 4096, stdin);

		if (strcmp("add\n", input) == 0) {
			esdb_act(db, (void*)add, 1, components);
		} else if (strcmp("make\n", input) == 0) {
			int *data = esdb_alloc(db, sizeof(int));
			*data = 0;
			int *ids = esdb_alloc(db, sizeof(int));
			ids[0] = values;
			
			queue_entity(db, 1, ids, data);
		}

		swap_buffers(db);
		esdb_act(db, (void*)print, 1, components);
	}

    free_esdb(db);

	return 0;
}
