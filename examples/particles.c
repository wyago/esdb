#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <GLFW/glfw3.h>

#include <esdb.h>
#include <block_list.h>

struct vec2f {
	float x, y;
};

void move(struct esdb *db, long int entity_id, 
    struct vec2f const **to_read, struct vec2f **to_set) {
    struct vec2f pos = *to_read[0];
    struct vec2f vel = *to_read[1];
    pos.x += vel.x;
    pos.y += vel.y;

    float r = rand() / (float)RAND_MAX * 0.02f;

    //vel.x = vel.x * cos(r) - vel.y * sin(r);
    //vel.y = vel.x * sin(r) + vel.y * cos(r);

    if (pos.x < 1 && pos.x > -1 &&
        pos.y < 1 && pos.y > -1) {
        glVertex2f(pos.x, pos.y);
    }

    *to_set[0] = pos;
    *to_set[1] = vel;
}

int main(void) {
	GLFWwindow* window;

	srand(12);

	if (!glfwInit()) { return -1; }

	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    struct esdb *db = make_esdb(1, 1024);
    int positions = register_component(db, sizeof(struct vec2f));
    int velocities = register_component(db, sizeof(struct vec2f));

	float lastTime = glfwGetTime();

	while (!glfwWindowShouldClose(window)) {
		glClear(GL_COLOR_BUFFER_BIT);
		glBegin(GL_POINTS);
        
        int components[2] = { positions, velocities };
        esdb_act(db, (void*)move, 2, components);

		glEnd();

		if (!glfwGetKey(window, 'S')) {
            int i;
            for (i = 0; i < 10; ++i) {
                struct vec2f *data = esdb_alloc(db, sizeof(struct vec2f) * 2);
                struct vec2f *newPosition = data + 0;
                struct vec2f *newVelocity = data + 1;
                int *ids = esdb_alloc(db, 2 * sizeof(int));
                ids[0] = positions;
                ids[1] = velocities;
                
                newPosition->x = 0;
                newPosition->y = 0;
                newVelocity->x = 0.000001f;
                newVelocity->y = 0.000001f;
                
                queue_entity(db, 2, ids, data);
            }
		}

		float time = glfwGetTime();
		float delta = time - lastTime;
		lastTime = time;
		//printf("%f\n", 1.0f/delta);

		glfwSwapBuffers(window);
		glfwPollEvents();
		swap_buffers(db);
	}

	glfwTerminate();
	return 0;
}
