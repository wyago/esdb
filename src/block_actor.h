#ifndef BLOCK_ACTOR_H_INCLUDED
#define BLOCK_ACTOR_H_INCLUDED

struct esdb;
typedef void (*block_actor)(struct esdb*, long int entity_id, const void **to_read, void **to_set);

#endif
